angular.module('angular-price-format', []).directive('pformat', [function () {
  return {
    require: '?ngModel',
    link: function (scope, elem, attrs, ngModelCtrl) {

      if (!ngModelCtrl) {
        return;
      }

      ngModelCtrl.$formatters.unshift(function (a) {
        elem[0].value = parseFloat((ngModelCtrl.$modelValue * 100).toFixed(2));
        elem.priceFormat({
          prefix: (typeof attrs.prefix === 'undefined') ? '' : attrs.prefix,
          suffix: (typeof attrs.suffix === 'undefined') ? '' : attrs.suffix,
          centsSeparator: (typeof attrs.cents === 'undefined') ? ',' : attrs.cents,
          thousandsSeparator: (typeof attrs.thousands === 'undefined') ? '.' : attrs.thousands,
          centsLimit: (typeof attrs.decimals === 'undefined') ? 2 : attrs.decimals,
          limit: (typeof attrs.limit === 'undefined') ? '' : attrs.limit,
          allowNegative: (typeof attrs.negative === 'undefined') ? '' : attrs.negative,
          insertPlusSign: (typeof attrs.plus === 'undefined') ? '' : attrs.plus
        });
        return elem[0].value;
      });

      ngModelCtrl.$parsers.unshift(function (viewValue) {
        elem.priceFormat({
          prefix: (typeof attrs.prefix === 'undefined') ? '' : attrs.prefix,
          suffix: (typeof attrs.suffix === 'undefined') ? '' : attrs.suffix,
          centsSeparator: (typeof attrs.cents === 'undefined') ? ',' : attrs.cents,
          thousandsSeparator: (typeof attrs.thousands === 'undefined') ? '.' : attrs.thousands,
          centsLimit: (typeof attrs.decimals === 'undefined') ? 2 : attrs.decimals,
          limit: (typeof attrs.limit === 'undefined') ? '' : attrs.limit,
          allowNegative: (typeof attrs.negative === 'undefined') ? '' : attrs.negative,
          insertPlusSign: (typeof attrs.plus === 'undefined') ? '' : attrs.plus
        });

        return parseInt(elem.unmask(), 10) / 100;
      });
    }
  };
}]);
