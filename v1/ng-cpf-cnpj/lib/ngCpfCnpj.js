/* global angular, CPF, CNPJ */
(function(window){

  'use strict';

  var module = angular.module('ngCpfCnpj', []);

  function applyValidator(validator, validatorName, ctrl) {

    if( ctrl.$validators ) {

      // Angular >= 1.3
      ctrl.$validators[validatorName] = function(modelValue, viewValue) {
        var value = modelValue || viewValue;
        // bsantanna ajustando esta diretiva para validação disparar somente se a string de input for composta de
        // 11 digitos numericos sem formatação, isso foi necessário pois o plugin ui-mask estava afetando o resultado
        // dessa validação e ocasionando bugs [HMP-1839]
        var cpfLengthRegex = /^\d{11}$/;
        var cnpjLengthRegex = /^\d{14}$/;
        var valid = true;
        if (cpfLengthRegex.test(value)) {
          valid = (validator.isValid(value) || !value);
        } else if(cnpjLengthRegex.test(value)) {
          valid = (validator.isValid(value) || !value);
        }
        if(/^([0-9])\1*$/.test(value)) {
          valid = false;
        }
        return valid;
      };

    } else {

      // Angular <= 1.2
      ctrl.$parsers.unshift(function (viewValue) {
        var value = viewValue.replace(/\D/g, "");
        var valid = validator.isValid(value) || !value;
        ctrl.$setValidity(validatorName, valid);
        return (valid ? viewValue : undefined);
      });

    }
  }

  if( window.CPF ) {

    module.directive('ngCpf', function() {
      return {

        restrict: 'A',

        require: 'ngModel',

        link: function(scope, elm, attrs, ctrl) {
          applyValidator(CPF, "cpf", ctrl);
        }

      };
    });
  }

  if( window.CNPJ ) {

    module.directive('ngCnpj', function() {
      return {

        restrict: 'A',

        require: 'ngModel',

        link: function(scope, elm, attrs, ctrl) {
          applyValidator(CNPJ, "cnpj", ctrl);
        }

      };
    });
  }

})(this);
